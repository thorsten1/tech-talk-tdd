TDD TechTalk - 26.02.2018 - Thorsten Tillack
============================================

1. Um das Test-Projekt auszuführen müsst ihr folgendes ausführen:

   ```
   composer install
   ```

   Wenn nach den Serverparametern gefragt wird, dann einfach jedes Mal
   Enter drücken und den Default übernehmen.

2. Sollte PhpUnit noch nicht installiert sein, dann geht ihr in PhpStorm 
auf den Menüpunkt "PhpStorm -> Preferences".

3. Dort geht ihr auf "Languages & Framworks -> PHP -> Test Frameworks".

4. Im nun folgenden Fenster klickt ihr auf das Pluszeichen und wählt
"PHPUnit local".

5. Dann den Radiobutton "Use Composer autoloader" wählen und im Feld
"Path to script:" den Pfad zum Vendor Verzeichnis eintragen gefolgt von
"/autoload.php". (Man kann auch auf die 3 Punkte klicken und den die
autoload.php im Vendor Verzeichnis auswählen.



