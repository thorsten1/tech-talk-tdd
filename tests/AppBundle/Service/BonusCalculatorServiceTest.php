<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\BonusCalculatorService;
use PHPUnit\Framework\TestCase;

class BonusCalculatorServiceTest extends TestCase
{
    /**
     * @var BonusCalculatorService
     */
    private $bonusService;

    protected function setUp()
    {
        $this->bonusService = new BonusCalculatorService();
    }
    /**
     * @test
     */
    public function calculate2MonthAtWorkReturns4()
    {
        $monthAtWork = 2;
        $bonusService = new BonusCalculatorService();
        $bonus = $bonusService->calculate($monthAtWork);
        $expectedBonus = 4;
        $this->assertEquals($expectedBonus, $bonus);
    }

    /**
     * @test
     * @expectedException \TypeError
     */
    public function calculateShouldFail()
    {
        $monthAtWork = 'string';
        $bonusService = new BonusCalculatorService();
        $bonusService->calculate($monthAtWork);
    }

    /**
     * @test
     */
    public function calculateShouldReturnCorrectBonus()
    {
        $monthAtWork = 60;
        $calculatedBonus = $this->bonusService->calculate($monthAtWork);
        $expectedBonus = 100;
        $this->assertEquals($expectedBonus, $calculatedBonus);

        $this->assertEquals(98, $this->bonusService->calculate(49));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function calculateMonthNegativeThrowsException()
    {
        $this->bonusService->calculate(-1);
    }
}
