<?php
declare(strict_types = 1);

namespace AppBundle\Service;

/**
 * Class BonusCalculatorService
 *
 * @package AppBundle\Service
 */
class BonusCalculatorService
{
    protected const BONUS_MULTIPLIER = 2;
    protected const MAX_MONTH = 50;
    protected const MAX_BONUS = 100;

    /**
     * Calculate Bonus.
     *
     * @param int $monthAtWork
     *
     * @return int
     * @throws \InvalidArgumentException
     */
    public function calculate(int $monthAtWork): int
    {
        if ($monthAtWork < 0) {
            throw new \InvalidArgumentException('Value musst be positive');
        }
        if ($this->maximumMonthsReached($monthAtWork)) {
            return self::MAX_BONUS;
        }
        return $monthAtWork * self::BONUS_MULTIPLIER;
    }

    /**
     * BLA.
     *
     * @param int $month
     *
     * @return bool
     */
    protected function maximumMonthsReached(int $month): bool
    {
        return $month >= self::MAX_MONTH;
    }
}